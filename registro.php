<?php
	include('acceso_db.php'); // incluimos el archivo de conexión a la Base de Datos
    include('header.php');
?>
        <!--main class="mdl-layout__content">-->
            <div class="page-content">
                <div class="mdl-grid">
                    
                    <div class="mdl-cell mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
                        <div id="caja_registro">
                            
                            <h4>Registrese</h4>
                            <form action="register_user.php" method="post">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <label for="input_email" class="mdl-textfield__label">Email</label>
                                <input type="email" class="mdl-textfield__input" id="correo_reg" name="usuario_email" autocomplete="off" />
                                <span class="mdl-textfield__error">Ingrese un correo v&aacute;lido</span>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <label for="input_email" class="mdl-textfield__label">Usuario</label>
                                <input type="text" class="mdl-textfield__input" pattern="[A-Z,a-z ]*" id="nick_reg" name="usuario_nombre" autocomplete="off" />
                                <span class="mdl-textfield__error">Ingrese solo letras</span>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <label for="input_password" class="mdl-textfield__label">Ingrese su password</label>
                                <input type="password" pattern="[A-Z,a-z,0-9,.]*" class="mdl-textfield__input" id="pass_reg" name="usuario_clave" />
                                <span class="mdl-textfield__error">Numeros letras y punto</span>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <label for="input_password" class="mdl-textfield__label">Vuelva a ingresar su password</label>
                                <input type="password" pattern="[A-Z,a-z,0-9,.]*" class="mdl-textfield__input" id="pass2_reg" name="usuario_clave_conf" />
                                <span class="mdl-textfield__error">Numeros letras y punto</span>
                            </div>
                            <input type="submit" name="enviar" id="registro" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" value="Registrar" />
                            <!-- <button id="registro" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored">
                            Registro
                            </button> -->
                            <div id="msg-reg"></div>

                            </form>
                            
                        </div><!--caja-login--> 
                    </div><!--mdl-csll-->
                
                </div><!--mdl-grid-->
            </div><!--page content-->
        <!--/main>-->
<?php
    include('footer.php');
	?>