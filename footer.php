<footer class="mdl-mini-footer">
        <div class="mdl-mini-footer__left-section">
        <div class="mdl-logo"><img src="<?php echo URL; ?>/img/bloguero_mini.png"></div>
            <ul class="mdl-mini-footer__link-list">
                <li><a target="_blank" href="http://www.bloguero-ec.com/mantenimiento-y-reparacion-de-computadoras">Contáctenos</a></li>
                <li><a target="_blank" href="http://www.bloguero-ec.com/licencia">| Licencia de producto</a></li>
                <li><a target="_blank" href="http://www.bloguero-ec.com/politica-de-privacidad">| Términos & Políticas 2016</a></li>
            </ul>
        </div>
     </footer>
 
    </div><!--mdl-layout-->
 
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
    <script type="text/javascript" src="js/jquery.ui.shake.js"></script>
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.0/material.min.js"></script>
    <script src="https://js.pusher.com/3.1/pusher.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script>
 
    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;
 
    var pusher = new Pusher('eb4cf8934399c867cb43', {
      encrypted: true
    });
 
    var channel = pusher.subscribe('test_channel');
    channel.bind('my_event', function(data) {
      alert(data.message);
    });
  </script>
</body>
</html>