<?php
	session_start();
	include('acceso_db.php');
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<?php
	    if(empty($_SESSION['usuario_nombre'])) { // comprobamos que las variables de sesión estén vacías        
	?>
	        <!-- <form action="comprobar.php" method="post">
	            <label>Usuario:</label><br />
	            <input type="text" name="usuario_nombre" /><br />
	            <label>Contraseña:</label><br />
	            <input type="password" name="usuario_clave" /><br />
	            <a href="recuperar_contrasena.php">Recuperar contraseña</a><br />
	            <input type="submit" name="enviar" value="Ingresar" />
	        </form>   --> 

	        <?php
    include('header.php');
?>
        <!--main class="mdl-layout__content">-->
            <div class="page-content">
                <div class="mdl-grid">
                    
                    <div class="mdl-cell mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
                        <div id="caja_login">
                            
                            <h4>Inicie sesi&oacute;n</h4>
                            <form id="login" method="post" action="comprobar.php" >
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <label for="input_email" class="mdl-textfield__label">Usuario</label>
                                <input type="text" class="mdl-textfield__input" pattern="[A-Z,a-z ]*" id="nick_reg" name="usuario_nombre" autocomplete="off" />
                                <span class="mdl-textfield__error">Ingrese solo letras</span>
                            </div>
                        
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <label for="input_password" class="mdl-textfield__label">Password</label>
                                <input type="password" class="mdl-textfield__input" id="password" name="usuario_clave" />
                                <span class="mdl-textfield__error">�ngrese solo letras y n�meros</span>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label disabled">
                                <label for="input_password" class="mdl-textfield__label">Codigo de verificaci&oacute;n</label>
                                <input type="password" pattern="[A-Z,a-z,0-9]*" class="mdl-textfield__input" id="cod_ver" name="cod_ver" />
                                <span class="mdl-textfield__error">�ngrese solo letras y n�meros</span>
                            </div>
                            <input type="submit" name="enviar" id="registro" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" value="Login" />
                            <!-- <button id="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored">
                            Login
                            </button> -->
                            <div class="text-center">
                            	<br><a href="recuperar_contrasena.php">Recuperar contraseña</a><br />
                            </div>
                            <div id="msg"></div>

                            </form>
                            
                        </div><!--caja-login--> 
                    </div><!--mdl-csll-->
    
                </div><!--mdl-grid-->
            </div><!--page content-->
        <!--/main>-->    
        <?php
    include('footer.php');
?>                 
	<?php
	    }else {
	?>
	        <p>Hola <strong><?=$_SESSION['usuario_nombre']?></strong> | <a href="logout.php">Salir</a></p>
	<?php
	    }
	?>  
</body>
</html>

